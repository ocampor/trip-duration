import numpy as np
import pandas as pd


def normalize(df):
    return (df - df.mean()) / df.std()


# rmsle(np.array, np.array) -> numpy.float64
def rmsle(predicted, real):
    if any(predicted < 0) or any(real < 0):
        raise RuntimeError("Logarithm of negative number or zero is not defined")
    log_predicted = np.log(predicted)
    log_real = np.log(real)
    return np.sqrt(np.mean(np.power(log_predicted - log_real, 2)))


def get_datetime_component(datetime_series, component):
    if datetime_series.dtype == 'object':
        datetime_series = pd.to_datetime(datetime_series)
    return getattr(datetime_series.dt, component)


def append_datetime_components(df, datetime_series, components):
    new_labels = []
    for component in components:
        new_label = '_'.join([datetime_series.name, component])
        df[new_label] = get_datetime_component(datetime_series, component)
        new_labels.append(new_label)
    return df, new_labels


def haversine(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2

    c = 2 * np.arcsin(np.sqrt(a))
    km = 6367 * c
    return km


def remove_outliers(df, variables, limit=3.5):
    selection = (df[variables] > -limit) & (df[variables] < limit)
    selection = selection.all(axis=1)
    return df[selection]


def append_dummy_variables(df, series, prefix):
    names = []
    dummies = pd.get_dummies(series, prefix=prefix)
    for name in dummies.columns:
        df[name] = dummies[name]
        names.append(name)
    return df, names


def merge_by_date(df1, df2, on=None, left_on=None, right_on=None, keep_datetime_variables=True):
    components = ['year', 'month', 'day', 'hour']
    if on is not None:
        assert type(on) is str
        left_on = on
        right_on = on
    elif (left_on is not None) and (right_on is not None):
        assert type(left_on) is str and type(right_on) is str

    df1, left_labels = append_datetime_components(df1, df1['pickup_datetime'], components)
    df2, right_labels = append_datetime_components(df2, df2['Time'], components)

    merged_data = pd.merge(df1, df2, left_on=left_labels, right_on=right_labels, how="left")

    if keep_datetime_variables:
        return merged_data

    return merged_data.drop(left_labels + right_labels, axis=1)
