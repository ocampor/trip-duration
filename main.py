import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split

import utils

###############################################################################################
# Read data
###############################################################################################
data = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')
weather = pd.read_csv('weather.csv')
###############################################################################################
# Get and process datetime components
###############################################################################################
datetime_components = ['hour', 'weekday', 'month']
data, data_datetime_lables = utils.append_datetime_components(data, data['pickup_datetime'],
                                                              datetime_components)
test, test_datetime_labels = utils.append_datetime_components(test, test['pickup_datetime'],
                                                              datetime_components)
# Merge weather
data = utils.merge_by_date(data, weather, 'pickup_datetime', 'Time', keep_datetime_variables=True)
test = utils.merge_by_date(test, weather, 'pickup_datetime', 'Time', keep_datetime_variables=True)
test = test.set_index(['id'])
# Change datetime compontents into categories
for component in data_datetime_lables:
    data[component] = data[component].astype('category')
    test[component] = test[component].astype('category')

###############################################################################################
# Calculate haversine distance
###############################################################################################
data['distance'] = utils.haversine(data['pickup_longitude'], data['pickup_latitude'],
                                   data['dropoff_longitude'], data['dropoff_latitude'])

test['distance'] = utils.haversine(test['pickup_longitude'], test['pickup_latitude'],
                                   test['dropoff_longitude'], test['dropoff_latitude'])

descriptive_variables = ['passenger_count', 'pickup_longitude', 'pickup_latitude',
                         'dropoff_longitude', 'dropoff_latitude', 'distance', 'Temp.',
                         'Humidity', 'Wind Speed', 'Gust Speed', 'Precip']


###############################################################################################
# Variables normalization
###############################################################################################
data = data.select_dtypes(exclude=[object, "datetime"])
X_train = utils.normalize(data[descriptive_variables])
y_train = np.log(data['trip_duration'])
X_train['trip_duration'] = y_train

X_test = test[descriptive_variables]
X_test = utils.normalize(X_test)

# Fill missing data
X_train = X_train.fillna(X_train.mean())
X_test = X_test.fillna(X_test.mean())

###############################################################################################
# Append dummy varaibles
###############################################################################################
# Change datetime compontents into dummy variables
for component in datetime_components:
    label = '_'.join(['pickup_datetime', component])
    X_train, names = utils.append_dummy_variables(X_train, data[label], prefix=component)
    X_test, names = utils.append_dummy_variables(X_test, test[label], prefix=component)

    # Add new variables to variable selection
    descriptive_variables += names

###############################################################################################
# Prepare training and testing samples
###############################################################################################
p = 0.70
y_ = X_train['trip_duration']
X = X_train[descriptive_variables]
X_train, X_val, y_train, y_val = train_test_split(X, y_, train_size=p)
##################
# xgboost
##################
# import xgboost as regressor
#
# dtrain = regressor.DMatrix(X_train, label=y_train)
# dvalid = regressor.DMatrix(X_val,label=y_val)
# dtest = regressor.DMatrix(X_test)
# watchlist = [(dtrain, 'train'), (dvalid, 'valid')]
# xgb_pars = {'min_child_weight': 50, 'eta': 0.3, 'colsample_bytree': 0.3, 'max_depth': 10,
#             'subsample': 0.8, 'lambda': 1., 'nthread': -1, 'booster' : 'gbtree', 'silent': 1,
#             'eval_metric': 'rmse', 'objective': 'reg:linear'}
# clf = regressor.train(xgb_pars, dtrain, 60, watchlist, early_stopping_rounds=50,
#       maximize=False, verbose_eval=10)
# y_val_hat = np.exp(clf.predict(dvalid))
# y_val_ = np.exp(y_val)
# print(utils.rmsle(y_val_hat, y_val_))
# y = clf.predict(dtest)
################


##################
# MLP Regressor
##################
# from sklearn.neural_network import MLPRegressor as regressor
#
# clf = regressor(verbose=True, hidden_layer_sizes=(100, 50, 10, 3), shuffle=True, max_iter=20,
#                 activation='relu')
# clf = clf.fit(X_train, y_train)
#
# y = np.exp(clf.predict(X_val))
# y_ = np.exp(y_val)
#
# print(utils.rmsle(y, y_))
# y = clf.predict(X_test)
#######

##################
# Random forest
##################
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score

clf = RandomForestRegressor(n_jobs=8, verbose=True)
clf = clf.fit(X_train, y_train)


y = np.exp(clf.predict(X_val))
y_ = np.exp(y_val)

print(utils.rmsle(y, y_))
y = clf.predict(X_test)
#######

##################
# Keras
##################
# from keras.models import Sequential
# from keras.layers.core import Dense, Activation
#
# model = Sequential([
#         Dense(100, input_shape=(70,)),
#         Activation('relu'),
#         Dense(100, input_shape=(100,)),
#         Activation('relu'),
#         Dense(100, input_shape=(100,)),
#         Activation('relu'),
#         Dense(100, input_shape=(100,)),
#         Activation('relu'),
#         Dense(1)
#     ])
#
# model.compile(loss="mean_squared_error", optimizer='rmsprop')
#
# model.fit(X_train.as_matrix(), y_train.as_matrix(), epochs=20, batch_size=2000)
# y = np.ndarray.flatten(model.predict(X_val.as_matrix()))
# y = np.exp(y)
# y_ = np.exp(y_val)
# print(utils.rmsle(y, y_))
# y = np.ndarray.flatten(model.predict(X_test.as_matrix()))
#######


ids = X_test.index
y = np.exp(y)
results = pd.DataFrame(y).set_index(ids).reset_index()
results.columns = ['id', 'trip_duration']

results.to_csv('results.csv', index=False)
