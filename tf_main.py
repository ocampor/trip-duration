import tensorflow as tf
import tf_utils
import utils
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split


data = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')

p = 0.7

data['pickup_datetime'] = pd.to_datetime(data['pickup_datetime'])
test['pickup_datetime'] = pd.to_datetime(test['pickup_datetime'])

data['pickup_hour'] = data['pickup_datetime'].apply(lambda t: t.hour)
test['pickup_hour'] = test['pickup_datetime'].apply(lambda t: t.hour)

descriptive_variables = ['pickup_hour', 'passenger_count', 'pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude']

X = utils.normalize(data[descriptive_variables])
X = np.abs(X)
y_ = np.log(data['trip_duration'])

X_train, X_test, y_train, y_test = train_test_split(X, y_, train_size=p)

x = tf.placeholder(tf.float32, [None, len(descriptive_variables)])
W = tf.Variable(tf.random_normal([len(descriptive_variables), 1], 0.4942, 0.1077))
b = tf.Variable(tf.random_normal([1], 0.1016, 0.01))

y = tf.matmul(x, W) + b

y_ = tf.placeholder(tf.float32, [None, 1])

loss_function = tf_utils.mse(y_, y)
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(loss_function)

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

# Train
for i in range(1000):
    p = 0.2
    Xs, _, ys, _ = train_test_split(X_train, y_train, train_size=p)
    batch_xs, batch_ys = Xs.as_matrix(), [[x.astype(np.float32)] for x in ys]
    _, loss = sess.run([train_step, loss_function], feed_dict={x: batch_xs, y_: batch_ys})
    if i % 5 == 0: print(loss)

W.eval().mean()
W.eval().std()

# Test trained model
y = sess.run(y, feed_dict={x: X_test.as_matrix(),
                             y_: np.array([[x] for x in y_test])})

y = np.array([np.exp(elem[0]) for elem in y])
y_test = np.exp(y_test)
rmsle(np.array(y_test), y)
