import tensorflow as tf

# rmsle(tensor, tensor) -> tensor
def rmsle(labels, predicted):
    log_labels = tf.log(tf.add(labels, 1.0))
    log_predicted = tf.log(tf.add(predicted, 1.0))
    return tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(log_predicted, log_labels))))

def mse(labels, predicted):
    return tf.reduce_mean(tf.square(tf.subtract(labels, predicted)))
